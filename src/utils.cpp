/** @file utils.cpp
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2014/12/13
 *
 *  logging and helpers
 */

#include "include/utils.h"

#include <QDateTime>
#include <QObject>
#include <QTextStream>
#if defined(Q_OS_ANDROID)
#include <android/log.h>
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
#define e Qt::endl
#else
#define e endl
#endif

void icc_examin_log( QString log_text )
{
    QTextStream s(stdout);
    s << "IccExamin("<< QDateTime::currentDateTime().toString("hh:mm:ss.zzz") <<"): "
      << log_text << e;
#if defined(Q_OS_ANDROID)
    __android_log_print(ANDROID_LOG_INFO, "IccExamin", "%s", log_text.toLocal8Bit().data() );
#endif
}

void icc_examin_log( QString file, QString func, QString log_text )
{
    QString t(file);
    int len = t.lastIndexOf('/');
    if(len > 0)
      t.remove( 0, len+1 );
    t += " " + func;

    QTextStream s(stdout);
    s << t << " " << QDateTime::currentDateTime().toString("hh:mm:ss.zzz") << " "
      << log_text << e;

#if defined(Q_OS_ANDROID)
    __android_log_print(ANDROID_LOG_INFO, t.toLocal8Bit().data(), "%s", log_text.toLocal8Bit().data() );
#endif
}

int iccExaminIsBigEndian ()
{ union { unsigned short u16; unsigned char c; } test = { .u16 = 1 }; return !test.c; }
