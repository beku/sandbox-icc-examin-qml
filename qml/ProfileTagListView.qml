/** @file ProfileTagListView.qml
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2015/11/16
 *
 *  view of the ICC color profile content list
 */

import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0

Rectangle {
    color: myPalette.window
    property int h
    property ListView list: view
    property string currentKey: "mediaWhitePoint"

    SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical

        Rectangle {
            id: mnftRect
            Layout.minimumHeight: parent.height/4.0
            width: parent.width
            height: parent.height/2.0
            //Layout.implicitHeight: parent.height/2.0
            color: "transparent"

            TextArea {
                id: textArea
                textColor: myPalette.windowText
                Accessible.name: "document"
                backgroundVisible: false // keep the area visually simple
                frameVisible: false      // keep the area visually simple
                anchors.fill: parent
                text: qsTr("Nothing loaded")
                textFormat: Qt.RichText
                wrapMode: TextEdit.Wrap
                readOnly: true
            }
        }

        ListModel {  id: listModel }

        ListView {
            id: view
            anchors.bottomMargin: 0
            anchors.top: mnftRect.bottom
            height: parent.height
            width: parent.width
            snapMode: ListView.SnapToItem
            highlightMoveDuration: 250
            clip: true
            keyNavigationWraps: true

            property string currentFileName: "dummy"
            function selectItem( pos )
            {
                view.currentIndex = pos
                if(typeof profileJsonObject === "undefined")
                    return

                var newFileName = profileJsonObject.FILE_name.value
                var newKey = profileJsonObject.TAG_data[pos].key
                if(currentFileName === newFileName &&
                   currentKey === newKey)
                    clearSubTagPreSelection = 1

                tagJsonObject = profileJsonObject.TAG_data[pos]
                currentKey = tagJsonObject.key
                currentFileName = newFileName

                pages.currentIndex = 0
                setPage(1)
            }

            highlight: Item {
                Rectangle {
                    width: parent.width
                    height: h
                    color: "lightsteelblue"
                }
            }
            model: listModel
            delegate: Rectangle {
                id: modelD
                width:  parent.width
                height: h
                color: "transparent"

                Rectangle {
                    id: mnftItemBackgroundRect
                    width:  view.width
                    anchors.bottom: modelD.bottom
                    anchors.left: modelD.left
                    height: 1
                    color: "gray"
                }
                Text {
                    id: mnftItemText
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.topMargin: h/8
                    anchors.leftMargin: h/4
                    anchors.left: parent.left
                    textFormat: Qt.RichText // Html
                    color: myPalette.windowText
                    text: "<b>" + key + "</b>&nbsp;&nbsp;" + " <i>" + value + "</i>"
                }

                MouseArea {
                    id: mnftItemMouseArea
                    anchors.fill: parent
                    onClicked: {
                        view.selectItem( index )
                    }
                }
            }
        }
    }

    function load( string_var ) {
        listModel.clear();
        textArea.text = ""
        statusText = "Load JSON" + " " + string_var.length
        profileJsonObject = JSON.parse( string_var )
        statusText = qsTr("Loaded JSON with length of:") + " " + string_var.length
        if(typeof profileJsonObject === "undefined" ||
           typeof profileJsonObject.FILE_name === "undefined")
            return

        var old_pos = loaded(profileJsonObject)
        view.currentIndex = old_pos

        if(profileJsonObject.TAG_data.length <= view.currentIndex)
            view.currentIndex = profileJsonObject.TAG_data.length - 1
        else if(view.currentIndex < 0)
            view.currentIndex = 0
        var pi = pages.currentIndex
        view.selectItem( view.currentIndex )
        // selectItem sets to the second page, which is not desireable
        pages.currentIndex = pi
        setPage(pi)
    }

    function loaded( jsonObject ) {
        //var loc = jsonObject.LOCALE_info.value
        var html
        html = "<html><body><p align=\"center\"><table border=\"0\" style=\"border-spacing:10px\">" +
                "<tr><td align=\"right\" style=\"padding-right:10;\">" + jsonObject.PROFILE_name.key_i18n + ":</td><td style=\"font-weight:bold;\">" + jsonObject.PROFILE_name.value + "</td></tr>" +
                "<tr><td align=\"right\" style=\"padding-right:10;\">" + jsonObject.FILE_name.key_i18n + ":</td><td>" + jsonObject.FILE_name.value + "</td></tr>" +
                "<tr><td align=\"right\" style=\"padding-right:10;\">" + jsonObject.FILE_size.key_i18n + ":</td><td>" + jsonObject.FILE_size.value + "</td></tr>"

        html += "<tr><td align=\"right\" style=\"padding-right:10;\">" + jsonObject.HEADER_name.key_i18n + "</td></tr>"
        for( var index in jsonObject.HEADER_data )
        {
            html += "<tr><td align=\"right\" style=\"padding-right:10;\">" + jsonObject.HEADER_data[index].key_i18n + ":</td><td style=\"font-weight:bold;\">" + jsonObject.HEADER_data[index].value + "</td></tr>"
        }

        red_x = red_y = green_x = green_y = blue_x = blue_y = white_x = white_y = black_x = black_y = -1
        curve_red = curve_green = curve_blue = curve_gray = typeof "undefined"
        html += "</table></p></body></html>";
        textArea.text = html
        var old_pos = view.currentIndex
        for( index in jsonObject.TAG_data )
        {
            if(jsonObject.TAG_data[index].key === currentKey)
                old_pos = index

            listModel.append({
                                 "key" : jsonObject.TAG_data[index].key_i18n,
                                 "value" : jsonObject.TAG_data[index].value,
                                 "jsonObject" : jsonObject.TAG_data[index]
                             });
            if(jsonObject.TAG_data[index].key === "redColorant")
            {
                var sum = jsonObject.TAG_data[index].X+jsonObject.TAG_data[index].Y+jsonObject.TAG_data[index].Z
                red_x = jsonObject.TAG_data[index].X/sum
                red_y = jsonObject.TAG_data[index].Y/sum
                redXYZ[0] = jsonObject.TAG_data[index].X
                redXYZ[1] = jsonObject.TAG_data[index].Y
                redXYZ[2] = jsonObject.TAG_data[index].Z
            }
            if(jsonObject.TAG_data[index].key === "greenColorant")
            {
                sum = jsonObject.TAG_data[index].X+jsonObject.TAG_data[index].Y+jsonObject.TAG_data[index].Z
                green_x = jsonObject.TAG_data[index].X/sum
                green_y = jsonObject.TAG_data[index].Y/sum
                greenXYZ[0] = jsonObject.TAG_data[index].X
                greenXYZ[1] = jsonObject.TAG_data[index].Y
                greenXYZ[2] = jsonObject.TAG_data[index].Z
            }
            if(jsonObject.TAG_data[index].key === "blueColorant")
            {
                sum = jsonObject.TAG_data[index].X+jsonObject.TAG_data[index].Y+jsonObject.TAG_data[index].Z
                blue_x = jsonObject.TAG_data[index].X/sum
                blue_y = jsonObject.TAG_data[index].Y/sum
                blueXYZ[0] = jsonObject.TAG_data[index].X
                blueXYZ[1] = jsonObject.TAG_data[index].Y
                blueXYZ[2] = jsonObject.TAG_data[index].Z
            }
            if(jsonObject.TAG_data[index].key === "mediaWhitePoint")
            {
                sum = jsonObject.TAG_data[index].X+jsonObject.TAG_data[index].Y+jsonObject.TAG_data[index].Z
                white_x = jsonObject.TAG_data[index].X/sum
                white_y = jsonObject.TAG_data[index].Y/sum
                whiteXYZ[0] = jsonObject.TAG_data[index].X
                whiteXYZ[1] = jsonObject.TAG_data[index].Y
                whiteXYZ[2] = jsonObject.TAG_data[index].Z
            }
            if(jsonObject.TAG_data[index].key === "mediaBlackPoint")
            {
                sum = jsonObject.TAG_data[index].X+jsonObject.TAG_data[index].Y+jsonObject.TAG_data[index].Z
                black_x = jsonObject.TAG_data[index].X/sum
                black_y = jsonObject.TAG_data[index].Y/sum
                blackXYZ[0] = jsonObject.TAG_data[index].X
                blackXYZ[1] = jsonObject.TAG_data[index].Y
                blackXYZ[2] = jsonObject.TAG_data[index].Z
            }
            if(jsonObject.TAG_data[index].key === "redTRC")
            {
                curve_red = jsonObject.TAG_data[index]
            }
            if(jsonObject.TAG_data[index].key === "greenTRC")
            {
                curve_green = jsonObject.TAG_data[index]
            }
            if(jsonObject.TAG_data[index].key === "blueTRC")
            {
                curve_blue = jsonObject.TAG_data[index]
            }
            if(jsonObject.TAG_data[index].key === "grayTRC")
            {
                curve_gray = jsonObject.TAG_data[index]
            }
            if(jsonObject.TAG_data[index].key === "colorantTable")
            {
                colorantTable = jsonObject.TAG_data[index]
            }
            if(jsonObject.TAG_data[index].key === "colorantTableOut")
            {
                colorantTableOut = jsonObject.TAG_data[index]
            }
        }
        return old_pos
    }
}

