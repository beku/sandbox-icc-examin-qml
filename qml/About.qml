/** @file About.qml
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2015/10/08
 *
 *  About info page
 */

import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.3

Rectangle {
    id: aboutRectangle
    width: parent.width
    height: parent.height
    anchors.centerIn: parent

    color: myPalette.window
    implicitHeight: parent.height
    implicitWidth: parent.width

    Rectangle {
        id: logoRectangle
        width: parent.width
        height: 350
        color: "lightgray"
        Image {
            id: logoImage
            width: parent.width
            height: parent.height
            anchors.centerIn: parent
            horizontalAlignment: Image.AlignHCenter
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/logo.svg"
            sourceSize.width: 350
            sourceSize.height: 350
        }
    }


    TextArea { // our content
        id: textArea
        y: logoImage.height + logoImage.y + 10
        width: parent.width
        height: parent.height - logoImage.height - font.pixelSize * 3 // keep some space for the button

        Accessible.name: "about text"
        backgroundVisible: false // keep the area visually simple
        frameVisible: false      // keep the area visually simple
        style: TextAreaStyle { textColor: myPalette.windowText }

        textFormat: Qt.RichText // Html
        textMargin: font.pixelSize
        readOnly: true // obviously no edits
        text: "<html><head></head><body> <p align=\"center\">" +
                      "Version " + ApplicationVersion + "<br \>" +
                      qsTr("ICC Version 2 and 4 compatible Color Profile Examine Tool") +
                      "<br \>Copyright (c) 2014-2017 Kai-Uwe Behrmann<br \>" +
                      qsTr("All Rights reserved.") +
                      "<br \><a href=\"http://www.behrmann.name\">www.behrmann.name</a></p>" +
                      "<hr /><p align=\"center\">" +
                      "<table border=\"0\" style=\"border-spacing:10px\">" +
                      "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Platform") + "</td>" +
                          "<td>" + Qt.platform.os + "</td></tr>" +
                      "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("System") + "</td>" +
                          "<td>" + SysProductInfo + "</td></tr>" +
                      "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Qt Version") + "</td>" +
                          "<td>" + QtRuntimeVersion + " (" + QtCompileVersion + ")</td></tr>" +
                      "<tr><td align=\"right\" style=\"padding-right:10;\">" + profile.getLibDescription(1) + "</td>" +
                          "<td>" + profile.getLibDescription(0) + "</td></tr>" +
                      "<tr><td align=\"right\" style=\"padding-right:10;\">" + profile.getLibDescription(2) + "</td>" +
                          "<td>" + profile.getLibDescription(3) + " ("+ profile.getLibDescription(3) + ")" + "</td></tr>" +
                      "</table></p>" +
                      "</body></html>"
        onLinkActivated: {
            setBusyTimer.start()
            if(Qt.openUrlExternally(link))
                statusText = qsTr("Launched app for ") + link
            else
                statusText = "Launching external app failed"
            unsetBusyTimer.start()
        }
        onLinkHovered: (Qt.platform.os === "android") ? Qt.openUrlExternally(link) : statusText = link
    }

    Action {
        id: aboutcloseAction
        text: qsTr("OK")
        shortcut: "Esc"
        onTriggered: {
            pages.currentIndex = 3
            setPage(-1)
        }
    }

    Button { // finish button
        text: qsTr("OK")
        width: parent.width - textArea.font.pixelSize * 2 // make this button big
        x: aboutRectangle.width/2 - width/2 // place in the middle
        y: aboutRectangle.height - textArea.font.pixelSize * 3 // place below aboutTextArea
        action: aboutcloseAction
    }
}

