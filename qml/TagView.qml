/** @file TagView.qml
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2015/11/16
 *
 *  view of the ICC color profile data
 */

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Layouts 1.0
import "color.js" as Color
import "linkify.js" as Link

Rectangle {
    width: parent.width
    height: parent.height
    color: "transparent"

    property int repaint: 0
    onRepaintChanged: canvas.requestPaint()
    property var viewJson
    property string html

    property int h
    property bool tagViewLongListMode: true

    ListModel {  id: tagItems }

    ListView {
        id: tagView
        anchors.top: parent.top
        width: parent.width
        height: ((tagItems.count * h) > (parent.height - 3 * buttons.height)) ? (parent.height - 3 * buttons.height) : tagItems.count * h
        snapMode: ListView.SnapToItem
        highlightMoveDuration: 250
        clip: true
        keyNavigationWraps: true

        property string currentKey: "dummy"

        function selectItem( pos )
        {
            tagView.currentIndex = pos
            if(typeof tagJsonObject === "undefined")
                return
            if(clearSubTagPreSelection)
            {
                clearSubTagPreSelection = 0
                tagView.currentIndex = -1
                currentKey = "dummy"
                return
            }

            viewJson = tagJsonObject.value_array[pos]
            if(typeof viewJson != "undefined")
            {
                if(typeof viewJson.key != "undefined" )
                    currentKey = viewJson.key
                else if(typeof viewJson.key_i18n != "undefined" )
                    currentKey = viewJson.key_i18n
            }

            repaint = !repaint
        }

        model: tagItems
        delegate: Rectangle {
            id: modelD
            width:  tagView.width
            height: h
            color: tagViewLongListMode ? Qt.rgba(r,g,b,t) : ListView.isCurrentItem ? "lightsteelblue" : "transparent"
            border.color: (L < 0.6) ? "lightsteelblue" : myPalette.windowText
            border.width: (tagViewLongListMode && ListView.isCurrentItem) ? 3 : 0

            Rectangle {
                id: mnftItemBackgroundRect
                width:  parent.width
                anchors.bottom: modelD.bottom
                anchors.left: modelD.left
                height: 1
                color: tagViewLongListMode ? "lightgray" : "gray"
            }
            Text {
                id: mnftItemText
                anchors.verticalCenter: parent.verticalCenter
                anchors.topMargin: h/8
                anchors.leftMargin: h/4
                anchors.left: parent.left
                textFormat: Qt.RichText // Html
                color: tagViewLongListMode ? (L < 0.6) ? "white" : "black" : myPalette.windowText
                text: "<b>" + key_i18n + "</b>&nbsp;&nbsp;" + " <i>" + value + "</i>"
            }

            MouseArea {
                id: mnftItemMouseArea
                anchors.fill: parent
                onClicked: {
                    tagView.selectItem( index )
                }
            }
        }
    }

    Rectangle {
        id: mnftRect
        width: parent.width
        height: parent.height - tagView.height
        anchors.top: tagView.bottom
        //Layout.minimumHeight: parent.height/4.0
        color: "transparent"

        TextArea { // our content
            id: textArea
            width: parent.width
            height: parent.height - font.pixelSize * 3 // keep some space for the button

            Accessible.name: "profile link"
            backgroundVisible: false // keep the area visually simple
            frameVisible: false      // keep the area visually simple

            textFormat: Qt.RichText // Html
            textMargin: font.pixelSize
            readOnly: true // obviously no edits
            text: html
            style: TextAreaStyle { textColor: myPalette.windowText }
            onLinkActivated: {
                setBusyTimer.start()
                if(Qt.openUrlExternally(link))
                    statusText = qsTr("Launched app for ") + link
                else
                    statusText = "Launching external app failed"
                unsetBusyTimer.start()
            }
            onLinkHovered: (Qt.platform.os === "android") ? Qt.openUrlExternally(link) : statusText = link
        }
        ComboBox {
            id:channelBox
            visible: false
            x: sm/4
            y: sm/4
            onActivated: slider.value = sliders[index]
        }
        Slider {
            id: slider
            x: sm * 2.5 + channelBox.width
            y: sm/4
            width: textArea.width - sm * 2.5 - channelBox.width - 1.5 * sm
            height: tm
            visible: false
            tickmarksEnabled: true
            stepSize: 1
            minimumValue: 1
            onValueChanged: repaint = !repaint
        }
        Text {
            visible: slider.visible
            text: slider.value
            x: slider.x + slider.width + tm/8
            y: slider.y
            color: myPalette.windowText
        }

        Label {
            id: coordLabel
            visible: false
            x: lm
            anchors.bottom: textArea.bottom
            width: 3*sm
            height: 1.35*sm
            opacity: showJson ? 0.3 : 1.0
            color: myPalette.windowText
        }

        Row {
            id: chanButtonsRow
            visible: false
            x: lm
            anchors.bottom: textArea.bottom
            width: parent.width - lm
            height: 1.5*sm

            Repeater {
                id: chanButtons
                model: 16
                Button {
                    visible: channels[index]
                    property string desc: "-"
                    property color textColor: myPalette.windowText
                    property color bgColor: "transparent"
                    onClicked: {
                        channels[index] = channels[index] ? 0 : 1
                        var count = 0
                        for(var i = 0; i < 16; ++i)
                        {
                            if(i == index)
                                continue
                            if(channels[i])
                            {
                                ++count
                                if(count >= 3)
                                    channels[i] = 0
                            }
                        }

                        repaint = !repaint
                    }

                    style: ButtonStyle {
                        background: Rectangle {
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: bgColor }
                                GradientStop { position: 0.25 ; color: "transparent" }
                            }
                            opacity: showJson ? 0.3 : 1.0
                        }
                        label: Text {
                            text: control.desc
                            color: textColor
                            opacity: showJson ? 0.3 : 1.0
                        }
                    }
                }
            }
        }

        RowLayout {
            id: buttons
            width: parent.width
            height: textArea.font.pixelSize * 3
            anchors.top: textArea.bottom
            anchors.left: textArea.left

            ToolButton {
                id: backButton
                enabled: !landscape
                style: ButtonStyle {
                    background: Rectangle {
                        id: nextTagButtonRect
                        border.width: enabled ? 2 : 0
                        border.color: "#888"
                        color: control.pressed ? myPalette.button : myPalette.window
                        radius: 4
                    }
                    label: Text {
                        text: enabled ? "❮" : ""
                        color: myPalette.windowText
                    }
                }
                onClicked: {
                    pages.currentIndex = 1
                    setPage(0)
                }
            }
            ToolButton {
                id: nextTagButton
                style: ButtonStyle {
                    background: Rectangle {
                        id: nextTagButtonRect
                        border.width: control.activeFocus ? 2 : 1
                        border.color: "#888"
                        color: control.pressed ? myPalette.button : myPalette.window
                        radius: 4
                    }
                    label: Text {
                        text: "▼"
                        color: myPalette.windowText
                    }
                }
                onClicked: {
                    tagListView.list.incrementCurrentIndex()
                    tagListView.list.selectItem( tagListView.list.currentIndex )
                }
            }
            ToolButton {
                id: previousTagButton
                style: ButtonStyle {
                    background: Rectangle {
                        id: nextTagButtonRect
                        border.width: control.activeFocus ? 2 : 1
                        border.color: "#888"
                        color: control.pressed ? myPalette.button : myPalette.window
                        radius: 4
                    }
                    label: Text {
                        text: "▲"
                        color: myPalette.windowText
                    }
                }
                onClicked: {
                    tagListView.list.decrementCurrentIndex()
                    tagListView.list.selectItem( tagListView.list.currentIndex )
                }
            }
            ToolButton { // detail button
                id: detailsButton
                style: ButtonStyle {
                    background: Rectangle {
                        id: nextTagButtonRect
                        border.width: control.activeFocus ? 2 : 1
                        border.color: "#888"
                        color: control.pressed ? myPalette.button : myPalette.window
                        radius: 4
                    }
                    label: Text {
                        text: showJson ? qsTr("Show Less") : qsTr("Show More")
                        color: myPalette.windowText
                    }
                }
                onClicked: {
                    showJson = !showJson
                    canvas.requestPaint()
                }
            }
        }
    }

    TextMetrics {
        id: textMetrics
        font.family: textArea.font
    }

    function load( ) {
        tagItems.clear();
        if(typeof tagJsonObject === "undefined")
            return

        var old_pos = loaded( tagJsonObject )

        if(old_pos >= 0)
            tagView.selectItem( old_pos )
    }

    function loaded( j )
    {
        viewJson = j

        if( typeof j.value_array != "object" ||
            typeof j.subelements_compound != "undefined" )
            return -1

        var a = j.value_array
        var old_pos = tagView.currentIndex
        var count = 0
        var pattern = tagView.currentKey
        for( var index in a )
        {
            if( typeof a[index] != "object" )
                continue

            var nc = a[index]
            var L = 1.00
            var r = 0.95
            var g = 0.95
            var b = 0.95
            var t = 0.00
            tagViewLongListMode = false
            if(typeof nc.L != "undefined")
            {
                var lab = [nc.L, nc.a, nc.b]
                var rgb = profile.getColor(lab,"web")
                L = nc.L
                r = rgb[0]
                g = rgb[1]
                b = rgb[2]
                t = 1.00
                tagViewLongListMode = true
            }
            var key = a[index].key
            if(typeof key === "undefined")
                key = a[index].key_i18n
            tagItems.append({
                                 "key" : key,
                                 "key_i18n" : a[index].key_i18n,
                                 "value" : a[index].value,
                                 "jsonObject" : a[index],
                                 "L" : L,
                                 "r" : r,
                                 "g" : g,
                                 "b" : b,
                                 "t" : t
                             });
            if(key === pattern)
                old_pos = index
            ++count
        }
        if(count === 0 ||
           old_pos >= count)
            old_pos = -1
        return old_pos
    }

    property bool showJson: false
    function parseMyJson( json ) {
        html = "<html><body>"
        var text = json.value

        // clean some markup from SampleICC
        if(json.key === "metaData") // dict
        {
            text = text.replace( "BEGIN DICT_TAG\n\n","" )
            text = text.replace( /BEGIN DICT_ENTRY\nName=/g, "" )
            text = text.replace( /\nValue=/g, ": " )
            text = text.replace( /\nEND DICT_ENTRY\n/g, "" )
            text = text.replace( "\nEND DICT_TAG", "" )
        }
        if(json.type === "colorantTable") // colr
        {
            text = text.replace( "BEGIN_COLORANTS 3\n", "" )
            text = text.replace( "BEGIN_COLORANTS 4\n", "" )
            text = text.replace( "BEGIN_COLORANTS 5\n", "" )
            text = text.replace( "BEGIN_COLORANTS 6\n", "" )
            text = text.replace( "BEGIN_COLORANTS 7\n", "" )
            text = text.replace( "# NAME Lab_L Lab_a Lab_b\n", "" )
            text = text.replace( "0 \"", "" )
            text = text.replace( "1 \"", "" )
            text = text.replace( "2 \"", "" )
            text = text.replace( "3 \"", "" )
            text = text.replace( /\"/g, "" )
            text = text.replace( "Red", qsTr("Red") )
            text = text.replace( "Green", qsTr("Green") )
            text = text.replace( "Blue", qsTr("Blue") )
            text = text.replace( "Cyan", qsTr("Cyan") )
            text = text.replace( "Magenta", qsTr("Magenta") )
            text = text.replace( "Yellow", qsTr("Yellow") )
            text = text.replace( "Black", qsTr("Black") )
            text = text.replace( "Orange", qsTr("Orange") )

            text = ""
            var colr_a = json.value_array
            for( var colr_index in colr_a )
                text = text + qsTr(colr_a[colr_index]) + "\n"
        }
        if(json.type === "profileSequenceIdentifier") // psid
        {
            text = text.replace( "BEGIN ProfileSequenceIdentification_TAG\n", "" )
            text = text.replace( /ProfileID:\n/g, "" )
            text = text.replace( /Description:\n/g, "" )
            text = text.replace( /ProfileDescription_/g, qsTr("Profile") + " " )
            text = text.replace( /Language = /g, "" )
            text = text.replace( "END ProfileSequenceIdentification_TAG", "" )
        }
        if(json.type === "s15Fixed16 Array")
        {
            text = text.slice(0, text.lastIndexOf("ArrayForm:"))
            text = text.replace( "Matrix Form:\n", "" )
        }
        if(json.type === "chromaticity")
        {
            text = text.replace( "Colorant Encoding : Customized Encoding\n", "" )
            text = text.replace( "Number of Channels : 3\n", "" )
            text = text.replace( /value\[.\]: /g, "" )
            text = text.replace( /, y=/g, "   y=" )
        }
        if(json.type === "XYZArray" ||
           json.type === "XYZ ")
        {
            var t = "<b>" + text + "</b>"
            text = t
            if(json.X + json.Y + json.Z)
            {
                text += "\n"
                text += "<small>x="
                text += json.X/(json.X+json.Y+json.Z)
                text += " y="
                text += json.Y/(json.X+json.Y+json.Z)
                text += "</small>"
            }
        }
        if(json.type === "namedColor2")
        {
            var n = json.count
            text = "<b>" + qsTr("Count:") + "</b> " + n
            if(typeof json.subelements_compound != "undefined")
            {
                text += '\n<table border="0" style="border-spacing:0px" width="100%">'
                for(var index in json.value_array)
                {
                    var nc = json.value_array[index]
                    var lab = [nc.L, nc.a, nc.b]
                    var rgb = profile.getColor(lab,"web")
                    var cname = Qt.rgba(rgb[0], rgb[1], rgb[2], 1.0)
                    text += '<tr style="background-color: ' + cname + ';color:' + ((nc.L < 0.5) ? 'white' : 'black') + ';width:50%;"><td align="right" style="padding-right:10;font-weight:bold;width:50%;">' + nc.key_i18n + '</td><td>L=' + nc.L + " a=" + nc.a + " b=" + nc.b + "</td></tr>"
                }
                text += "</table>"
            }
        }

        if(typeof text === "undefined")
            text = qsTr("undefined")

        if( skipHeadline === 0 )
        {
            if(text.indexOf('<b>') >= 0)
            {   // accept HTML
                text = text.replace( /\n/g, "< br/>" )
                html += json.key_i18n + " :<div>" + text + "</div>"
            }
            else
            {   // assume text or xml to display
                text = text.replace( /</g, "&lt;" )
                text = text.replace( />/g, "&gt;" )
                text = text.replace( / /g, "&nbsp;" )
                text = text.replace( /\n/g, "< br/>" )
                html += json.key_i18n + " :<div style=\"font-weight:bold\">" + text + "</div>"
            }
        }
        if(showJson)
        {
            html += "<p align=\"left\"><table border=\"0\" style=\"border-spacing:10px\">"
            html += "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Details") + "</td></tr>"
            for( var obj in json )
            {
                html += "<tr><td align=\"right\" style=\"padding-right:10;\">" + obj + ":</td><td>" + json[obj] + "</td></tr>"
            }
            html += "</table>"
        }
        html += "</body></html>";
        var lhtml = Link.linkify( html );
        html = lhtml;
    }

    function colorFromChannelName(channel_name, i)
    {
        var desc = channel_name
        var nick = channel_name
        var color = myPalette.windowText
        var textColor = myPalette.windowText
        if(channel_name === "RGB_R")
        {
            nick = "R"
            desc = qsTr("Red")
            textColor = color = "red"
        } else if(channel_name === "RGB_G")
        {
            nick = "G"
            desc = qsTr("Green")
            textColor = color = "green"
        } else if(channel_name === "RGB_B")
        {
            nick = "B"
            desc = qsTr("Blue")
            textColor = Qt.lighter("blue", 1.3)
            color = "blue"
        } else if(channel_name === "CMYK_C")
        {
            nick = "C"
            desc = qsTr("Cyan")
            textColor = color = "cyan"
        } else if(channel_name === "CMYK_M")
        {
            nick = "M"
            desc = qsTr("Magenta")
            textColor = color = "magenta"
        } else if(channel_name === "CMYK_Y")
        {
            nick = "Y"
            desc = qsTr("Yellow")
            textColor = color = "yellow"
        } else if(channel_name === "CMYK_K")
        {
            nick = "K"
            desc = qsTr("Black")
            textColor = color = myPalette.windowText
        } else if(channel_name === "Lab_L")
        {
            nick = "L"
            desc = qsTr("CIE*L")
            textColor = color = "gray"
        } else if(channel_name === "Lab_a")
        {
            nick = "a"
            desc = qsTr("CIE*a")
            textColor = color = "red"
        } else if(channel_name === "Lab_b")
        {
            nick = "b"
            desc = qsTr("CIE*b")
            textColor = Qt.lighter("blue", 1.3)
            color = "blue"
        } else if(channel_name === "XYZ_X")
        {
            nick = "X"
            desc = qsTr("CIE*X")
            textColor = color = "red"
        } else if(channel_name === "XYZ_Y")
        {
            nick = "Y"
            desc = qsTr("CIE*Y")
            textColor = color = "green"
        } else if(channel_name === "XYZ_Z")
        {
            nick = "Z"
            desc = qsTr("CIE*Z")
            textColor = Qt.lighter("blue", 1.3)
            color = "blue"
        } else if(channel_name === "YCbr_1")
        {
            nick = desc = "Y"
            textColor = color = "gray"
        } else if(channel_name === "YCbr_2")
        {
            nick = desc = "Cb"
            textColor = Qt.lighter("blue", 1.3)
            color = "blue"
        } else if(channel_name === "YCbr_3")
        {
            nick = desc = "Cr"
            textColor = color = "red"
        } else if(channel_name === "gamt")
        {
            nick = desc = "K"
            textColor = color = myPalette.windowText
        } else if( typeof colorantTable != "undefined" &&
                   typeof colorantTable.value_array != "undefined" )
        {
            desc = colorantTable.value_array[i]
            if(desc === "Red") {
                nick = "R"
                textColor = color = "red"
            } else if(desc === "Green") {
                nick = "G"
                textColor = color = "green"
            } else if(desc === "Blue") {
                nick = "B"
                textColor = Qt.lighter("blue", 1.3)
                color = "blue"
            } else if(desc === "Cyan") {
                nick = "C"
                textColor = color = "cyan"
            } else if(desc === "Magenta") {
                nick = "M"
                textColor = color = "magenta"
            } else if(desc === "Yellow") {
                nick = "Y"
                textColor = color = "yellow"
            } else if(desc === "Black") {
                nick = "K"
                textColor = color = myPalette.windowText
            } else if(desc === "Orange") {
                nick = "O"
                textColor = color = "orange"
            } else if(desc === "White") {
                nick = "W"
                textColor = color = "white"
            }
            desc = qsTr(desc)
        }
        return [desc,nick,color,textColor]
    }

    function drawXYZ( ctx, strokeStyle, fillStyle, lineWidth )
    {
        ctx.save()

        ctx.strokeStyle = strokeStyle
        ctx.fillStyle = fillStyle
        ctx.lineWidth = lineWidth/2.0
        ctx.translate( lm, height - d - 2.5*sm )

        drawDecoration( ctx, projection_xyz )

        //! sprectra begin
        drawSpectra( ctx, projection_xyz )
        //! sprectra end


        //! EDID triangle begin
        ctx.strokeStyle = "black"
        ctx.lineWidth = lineWidth
        if(showJson === false)
            ctx.globalAlpha = 0.5
        else
            ctx.globalAlpha = 0.3
        if( ( red_x != -1 && green_x != -1 && blue_x != -1 &&
              projection_xyz === true ) ||
            drawSaturation( ctx, projection_xyz )
          )
        {
            ctx.beginPath()
            ctx.moveTo( xToImage( xFromXYZ(redXYZ, projection_xyz)),
                        yToImage( yFromXYZ(redXYZ, projection_xyz)) )
            ctx.lineTo( xToImage( xFromXYZ(greenXYZ, projection_xyz)),
                        yToImage( yFromXYZ(greenXYZ, projection_xyz)) )
            ctx.lineTo( xToImage( xFromXYZ(blueXYZ, projection_xyz)),
                        yToImage( yFromXYZ(blueXYZ, projection_xyz)) )
            ctx.closePath()
        }
        ctx.fill()
        if(showJson === false)
            ctx.globalAlpha = 1.0
        ctx.strokeStyle = myPalette.windowText
        ctx.stroke()

        if(showJson === false)
            ctx.globalAlpha = 1
        else
            ctx.globalAlpha = 0.5
        var XYZ = [0,0,0]
        var rgb = [0,0,0]
        if(white_x != -1)
        {
            ctx.strokeStyle = "white"
            rgb = [1,1,1]
            if( projection_xyz === true )
                XYZ = whiteXYZ
            else
                XYZ = profile.getAbsoluteXYZ(rgb)
            ctx.lineWidth = lineWidth/2.0
            ctx.beginPath()
            ctx.moveTo( xToImage( xFromXYZ(XYZ, projection_xyz))-sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))-sm/2.0)
            ctx.lineTo( xToImage( xFromXYZ(XYZ, projection_xyz))+sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))+sm/2.0)
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo( xToImage( xFromXYZ(XYZ, projection_xyz))+sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))-sm/2.0)
            ctx.lineTo( xToImage( xFromXYZ(XYZ, projection_xyz))-sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))+sm/2.0)
            ctx.stroke()
            ctx.strokeStyle = canvas.strokeStyle
        }
        if(black_x != -1)
        {
            ctx.strokeStyle = myPalette.windowText
            ctx.strokeStyle = "black"
            rgb = [0,0,0]
            if( projection_xyz === true )
                XYZ = whiteXYZ
            else
                XYZ = profile.getAbsoluteXYZ(rgb)
            ctx.lineWidth = lineWidth/2.0
            ctx.beginPath()
            ctx.moveTo( xToImage( xFromXYZ(XYZ, projection_xyz))-sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))-sm/2.0)
            ctx.lineTo( xToImage( xFromXYZ(XYZ, projection_xyz))+sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))+sm/2.0)
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo( xToImage( xFromXYZ(XYZ, projection_xyz))+sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))-sm/2.0)
            ctx.lineTo( xToImage( xFromXYZ(XYZ, projection_xyz))-sm/2.0,
                        yToImage( yFromXYZ(XYZ, projection_xyz))+sm/2.0)
            ctx.stroke()
            ctx.strokeStyle = canvas.strokeStyle
        }
        ctx.lineWidth = lineWidth
        // draw small circle
        if(viewJson.key === "redColorant")
            ctx.fillStyle = "red"
        else if(viewJson.key === "greenColorant")
            ctx.fillStyle = "green"
        else if(viewJson.key === "blueColorant")
            ctx.fillStyle = "blue"
        else if(viewJson.key === "mediaWhitePoint")
            ctx.fillStyle = "white"
        else if(viewJson.key === "mediaBlackPoint")
            ctx.fillStyle = "black"
        ctx.beginPath()
        // we need rgb input, ICC XYZ input is not precise
        if(viewJson.key === "mediaWhitePoint")
        {
            rgb = [1,1,1]
            XYZ = profile.getAbsoluteXYZ(rgb)
        } else if(viewJson.key === "redColorant")
        {
            rgb = [1,0,0]
            XYZ = profile.getAbsoluteXYZ(rgb)
        } else if(viewJson.key === "greenColorant")
        {
            rgb = [0,1,0]
            XYZ = profile.getAbsoluteXYZ(rgb)
        } else if(viewJson.key === "blueColorant")
        {
            rgb = [0,0,1]
            XYZ = profile.getAbsoluteXYZ(rgb)
        } else if(viewJson.key === "mediaBlackPoint")
        {
            rgb = [0,0,0]
            XYZ = profile.getAbsoluteXYZ(rgb)
        }

        if(viewJson.type === "XYZArray" ||
           viewJson.type === "XYZ ")
        {
            if( projection_xyz === true )
                XYZ = [viewJson.X,viewJson.Y,viewJson.Z]

            ctx.arc( xToImage( xFromXYZ(XYZ, projection_xyz)),
                     yToImage( yFromXYZ(XYZ, projection_xyz)),
                     sm/2, 0, 2 * Math.PI, false);
            ctx.fill()
            ctx.stroke()
        } else
        if(viewJson.type === "chromaticity")
        {
            for(var i = 0; i < viewJson.count; ++i)
            {
                var x = viewJson.value_array[i].x
                var y = viewJson.value_array[i].y
                XYZ = [x,y,1.0-x-y]

                if(i === 0)
                    ctx.fillStyle = "red"
                else if(i === 1)
                    ctx.fillStyle = "green"
                else if(i === 2)
                    ctx.fillStyle = "blue"

                ctx.beginPath()
                ctx.arc( xToImage( xFromXYZ(XYZ, projection_xyz)),
                         yToImage( yFromXYZ(XYZ, projection_xyz)),
                         sm/2, 0, 2 * Math.PI, false);
                ctx.fill()
                ctx.stroke()
            }
        }
        //! EDID triangle end


        ctx.fillStyle = myPalette.windowText
        ctx.font = textArea.font.pixelSize + 'px sans-serif vertical-lr'
        if(showJson === false)
            ctx.fillStyle = myPalette.windowText
        if(projection_xyz) {
            ctx.fillText( "CIE*x", d-sm*2.5, d+sm )
            var t = "CIE*y"
            ctx.textAlign = "center"
            for(var i = 0; i < t.length; ++i)
                ctx.fillText( t[i], -sm/2, sm + sm*i )
            ctx.textAlign = "left"
        } else {
            ctx.fillText( "CIE*a", d-sm*2.5, d/2+sm )
            ctx.fillText( "CIE*b", d/2-2.5*sm, sm )
        }
        ctx.restore()
    }

    function drawCurves( ctx, json, describe )
    {
        if(typeof json != "object")
            return

        var count = json.value_array.length
        for(var i = 0; i < count; ++i)
        {
            var colors = colorFromChannelName(json.value_array[i].value, i)
            var desc = colors[0]
            var nick = colors[1] // nick name, one or two letters
            var color = colors[2]
            var textColor = colors[3]

            ctx.strokeStyle = color

            drawCurve( ctx, json.value_array[i], nick, i, color, textColor )

            // show description for each curve
            if( typeof json.value_array[i].description == "string" )
            {
                ctx.fillStyle = textColor
                var ty = d - textArea.font.pixelSize - count*tm/1.4 + i*tm/1.4
                // show description only, when it fits below clut element list
                if(textArea.height > d - ty + tm*1.1)
                    ctx.fillText( desc + " " + json.value_array[i].description,
                                  textArea.font.pixelSize/2, ty )
            }
        }
    }

    property real mouse_x: 0
    property real mouse_y: 0
    function drawCurve( ctx, json, describe, pos, textColor )
    {
        if(typeof json != "object")
            return

        if(typeof json.count != "number")
            return

        var count = json.count
        if(pos_x <= d)
            mouse_x = pos_x/d
        mouse_y = -1

        var i
        ctx.beginPath()
        // linear curve
        if((count === 1 && json.value_array[0] === 0) ||
            count === 0)
        {
            ctx.moveTo( 0, d )
            ctx.lineTo( d, 0 )
            ctx.stroke()
            mouse_y = mouse_x
        }
        else // gamma curve or parametric Type 0
        if((json.type === "curve" && count === 1) ||
           (json.type === "pcurve"&& json.curve_type === 0))
        {
            ctx.moveTo( 0, d )
            var n = 64.0
            var gamma = json.value_array[0]*256.0
            if(json.type === "pcurve")
                gamma = json.value_array[0]
            for(i = 1; i <= n; ++i)
                ctx.lineTo( i/n*d, d-Math.pow(i/n, gamma )*d )
            ctx.stroke()
            mouse_y = Math.pow( mouse_x, gamma )
        }
        else // parametric curve Type 1,2,3,4
        if(json.type === "pcurve")
        {
            n = 64.0
            var X
            var Y
            gamma = json.value_array[0]
            var A = 0
            var B = 0
            var C = 0
            var D = 0
            var E = 0
            var F = 0
            if(json.curve_type === 1)
            {
                A = json.value_array[1]
                B = json.value_array[2]
                for(i = 0; i <= n; ++i)
                {
                    X = i/n
                    if(X >= -B/A)
                        Y = Math.pow( A*X+B, gamma )
                    else
                        Y = 0
                    if(i === 0)
                        ctx.moveTo( X * d, d - Y * d )
                    else
                        ctx.lineTo( X * d, d - Y * d )
                }
                ctx.stroke()
                if(mouse_x >= -B/A)
                    mouse_y = Math.pow( A*mouse_x+B, gamma )
                else
                    mouse_y = 0
            } else
            if(json.curve_type === 2)
            {
                A = json.value_array[1]
                B = json.value_array[2]
                C = json.value_array[3]
                for(i = 0; i <= n; ++i)
                {
                    X = i/n
                    if(X >= -B/A)
                        Y = Math.pow( A*X+B, gamma ) + C
                    else
                        Y = C
                    if(i === 0)
                        ctx.moveTo( X * d, d - Y * d )
                    else
                        ctx.lineTo( X * d, d - Y * d )
                }
                ctx.stroke()
                if(mouse_x >= -B/A)
                    mouse_y = Math.pow( A*mouse_x+B, gamma ) + C
                else
                    mouse_y = C
            } else
            if(json.curve_type === 3)
            {
                A = json.value_array[1]
                B = json.value_array[2]
                C = json.value_array[3]
                D = json.value_array[4]
                for(i = 0; i <= n; ++i)
                {
                    X = i/n
                    if(X >= D)
                        Y = Math.pow( A*X+B, gamma )
                    else
                        Y = C*X
                    if(i === 0)
                        ctx.moveTo( X * d, d - Y * d )
                    else
                        ctx.lineTo( X * d, d - Y * d )
                }
                ctx.stroke()
                if(mouse_x >= D)
                    mouse_y = Math.pow( A*mouse_x+B, gamma )
                else
                    mouse_y = C*mouse_x
            } else
            if(json.curve_type === 4)
            {
                A = json.value_array[1]
                B = json.value_array[2]
                C = json.value_array[3]
                D = json.value_array[4]
                E = json.value_array[5]
                F = json.value_array[6]
                for(i = 0; i <= n; ++i)
                {
                    X = i/n
                    if(X >= D)
                        Y = Math.pow( A*X+B, gamma ) + E
                    else
                        Y = C*X+F
                    if(i === 0)
                        ctx.moveTo( X * d, d - Y * d )
                    else
                        ctx.lineTo( X * d, d - Y * d )
                }
                ctx.stroke()
                if(mouse_x >= D)
                    mouse_y = Math.pow( A*mouse_x+B, gamma ) + E
                else
                    mouse_y = C*mouse_x+F
            } else
            if(json.curve_type === 5) // parametric vcgt
            {
                A = json.value_array[1]
                B = json.value_array[2]
                var mult = B-A
                for(i = 0; i <= n; ++i)
                {
                    X = i/n
                    Y = mult * Math.pow( X, 1.0/gamma ) + A
                    if(i === 0)
                        ctx.moveTo( X * d, d - Y * d )
                    else
                        ctx.lineTo( X * d, d - Y * d )
                }
                ctx.stroke()
                mouse_y = mult * Math.pow( mouse_x, 1.0/gamma ) + A
            }
        }
        else // segmented curve
        {
            ctx.moveTo( 0, d-json.value_array[0]*d )
            for(i = 0; i < count; ++i)
                ctx.lineTo( i/(count-1)*d, d-json.value_array[i]*d )
            ctx.stroke()
            var segment_min = Math.floor(mouse_x*(count-1))
            if(segment_min >= count - 1)
                segment_min = count - 2
            var diff_y = json.value_array[segment_min+1] - json.value_array[segment_min]
            var segment_part_x = mouse_x - segment_min/count
            if(segment_min === 0 || segment_min >= 1)
                mouse_y = json.value_array[segment_min] + diff_y * segment_part_x

        }

        // mouse position
        ctx.beginPath()
        ctx.arc( mouse_x*d, d-mouse_y*d, sm/2, 0, 2 * Math.PI, false);
        ctx.stroke()
        ctx.font = textArea.font.pixelSize + 'px sans-serif vertical-lr'
        if(describe.length >= 1)
        {
            ctx.fillStyle = textColor
            ctx.fillText( describe + ": " + mouse_y.toFixed(3), pos * textArea.font.pixelSize * (describe.length+1+4) * 0.7, d+sm*1.5 )
        }
        if(pos === 0)
        {
            ctx.fillStyle = myPalette.windowText
            ctx.fillText( mouse_x.toFixed(3), d, d-sm )
        }
    }

    function activeClutColor( vctx, xy_pos )
    {
        var mark = ["red","green",Qt.lighter("blue", 1.3),myPalette.windowText, "transparent"]
        var count = 0
        var c = channels
        var i
        var channelMark = []
        for(i = 0; i < vctx.clut.value_out && count < 3; ++i)
        {
            if(c[i])
            {
                vctx.color[count] = vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]]
                ++count
            }
        }

        var pos = 0
        for(i = 0; i < 16; ++i)
        {
            if(c[i])
            {
                if(count === 1)
                {
                    channelMark[i] = mark[3]
                    vctx.color[0] = vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]]
                    vctx.color[1] = vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]]
                    vctx.color[2] = vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]]
                } else if(count)
                    channelMark[i] = mark[pos]
                ++pos
            } else
                channelMark[i] = mark[4]
        }
        return channelMark
    }


    function clutOffset( channels_in, channels_in_n, grid, channels_out_n, dimensions_out )
    {
        var i
        var table_size = grid[0] * channels_out_n

        for(i = 1; i < dimensions_out; ++i)
            table_size *= grid[i]

        var start = 0
        for(i = dimensions_out; i < channels_in_n; ++i)
        {
          start += table_size * (channels_in[i] <= 0 ? 0 : channels_in[i]);
          table_size *= grid[i];
        }
        return start;
    }

    property var sliders: [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    property var channels:[1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0]

    function drawClut( ctx, lineWidth, fillStyle, strokeStyle )
    {
        var vctx = {} // viewer context
        vctx.channels_out = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        vctx.coord        = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        vctx.grid         = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        vctx.clut = viewJson

        var i
        for(i = 0; i < 16; ++i) {
            vctx.channels_out[i] = 0;
            vctx.grid[i] = 0.0
            vctx.coord[i] = 0
        }
        for(i = 0; i < vctx.clut.value_out; ++i)
            vctx.channels_out[i] = i;
        for(i = 0; i < vctx.clut.value_in; ++i)
            vctx.grid[i] = vctx.clut.values_gridpoints[i]

        var cindex = channelBox.currentIndex < (vctx.clut.value_in-1 - 2) ? channelBox.currentIndex : vctx.clut.value_in-1 - 2
        if(vctx.clut.value_in > 2)
        {
            slider.visible = true
            slider.minimumValue = 1
            slider.maximumValue = vctx.grid[cindex+2]

            channelBox.visible = true

            sliders[cindex] = slider.value
        }

        vctx.channel_names = [""]
        for(i = 2; i < vctx.clut.value_in; ++i)
        {
            var cname = vctx.clut.values_channels_names_in[i]
            var colors = colorFromChannelName(cname,i)
            vctx.channel_names[i-2] = colors[0]
            vctx.coord[i] = sliders[i-2] - 1
        }
        channelBox.model = vctx.channel_names
        channelBox.currentIndex = cindex
        ctx.font = textArea.font.pixelSize + 'px sans-serif vertical-lr'
        textMetrics.font = ctx.font
        if(vctx.clut.value_in > 2)
        {
            textMetrics.text = vctx.channel_names[cindex]
            channelBox.width = textMetrics.width + lm + tm
        }

        slider.value = sliders[cindex]

        var margin = 0.025*tm
        if(d/vctx.grid[0]/20 < 1.0)
            margin = -d/vctx.grid[0]/20
        var x
        var y
        vctx.color = [0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0]
        // draw 2D rectangles
        var base_pos = clutOffset( vctx.coord, vctx.clut.value_in, vctx.grid, vctx.clut.value_out, 2 )
        var xy_pos
        for(x = 0; x < vctx.grid[0]; ++x)
        {
            vctx.coord[0] = x
            for(y = 0; y < vctx.grid[1]; ++y)
            {
                vctx.coord[1] = y
                xy_pos = y*vctx.grid[0]*vctx.clut.value_out + x*vctx.clut.value_out + base_pos
                activeClutColor( vctx, xy_pos )
                var rgba = Qt.rgba(vctx.color[0], vctx.color[1], vctx.color[2], ctx.globalAlpha)
                ctx.fillStyle = rgba
                ctx.fillRect(d/vctx.grid[0]*x + margin,
                             d-d/vctx.grid[1]*(y+1) + margin,
                             d/vctx.grid[0] - 2*margin,
                             d/vctx.grid[1] - 2*margin)
            }
        }

        if(inside)
        {
            chanButtonsRow.visible = true
            coordLabel.visible = true

            if(pos_x <= d)
                mouse_x = pos_x/d
            if(pos_y <= d)
                mouse_y = (d-pos_y)/d
            ctx.fillStyle = "black"
            vctx.coord[0] = Math.min((mouse_x*vctx.grid[0]+0.5).toFixed(0)-1, vctx.grid[0]-1)
            if(vctx.coord[0] < 0) vctx.coord[0] = 0
            vctx.coord[1] = Math.min((mouse_y*vctx.grid[1]+0.5).toFixed(0)-1, vctx.grid[1]-1)
            if(vctx.coord[1] < 0) vctx.coord[1] = 0
            var color_desc
            color_desc = (((vctx.coord[0]+1)<10)?"0":"") + (vctx.coord[0]+1)
            for(i = 1; i < vctx.clut.value_in; ++i)
                color_desc += "*" + (((vctx.coord[i]+1)<10)?"0":"") + (vctx.coord[i]+1)
            // print coordinates of hovered reactangle
            textMetrics.text = color_desc
            textMetrics.font = coordLabel.font
            coordLabel.text = textMetrics.text
            coordLabel.width = d+sm*1.5
            var cw = textArea.font.pixelSize * 0.35
            var tw = textMetrics.width + cw
            chanButtonsRow.x = lm + tw
            chanButtonsRow.width = textArea.width - lm - tw
            xy_pos = vctx.coord[1]*vctx.grid[0]*vctx.clut.value_out + vctx.coord[0]*vctx.clut.value_out + base_pos
            // print output values of hoverd reactangle
            var markColors = activeClutColor( vctx, xy_pos )
            for(i = 0; i < vctx.clut.value_out; ++i)
            {
                vctx.color[i] = vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]]
                cname = vctx.clut.values_channels_names_out[i]
                colors = colorFromChannelName(cname,i)
                var describe = colors[1] + ": " + (typeof vctx.color[i] != "undefined" ? vctx.color[i].toFixed(3) : -1)
                ctx.fillStyle = colors[2]
                textMetrics.text = colors[1] + ": 0,999"
                chanButtons.itemAt(i).desc = describe
                chanButtons.itemAt(i).textColor = colors[3]
                chanButtons.itemAt(i).visible = true
                chanButtons.itemAt(i).bgColor = markColors[i]
                tw += textMetrics.width + cw
            }
            for(; i < 16; ++i)
                chanButtons.itemAt(i).visible = false

            // emphasize the hoverd rectangle
            for(i=0; i < 16; ++i) vctx.color[i] = 0
            activeClutColor( vctx, xy_pos )
            rgba = Qt.rgba(vctx.color[0], vctx.color[1], vctx.color[2], ctx.globalAlpha)
            ctx.fillStyle = rgba
            var bottom = ((vctx.coord[1]) === 0)
            var bmargin = d/vctx.grid[0]/2
            if(bmargin < textArea.font.pixelSize)
                bmargin = textArea.font.pixelSize

            var rx = d/vctx.grid[0]  *vctx.coord[0]     - bmargin
            var ry = d-d/vctx.grid[1]*(vctx.coord[1]+1) - bmargin
            var rw = d/vctx.grid[0] + 2*bmargin
            var rh = d/vctx.grid[1] + 2*bmargin - (bottom ? bmargin - textArea.font.pixelSize/2 : 0)
            ctx.lineWidth = lineWidth/3.0
            ctx.fillRect(rx, ry, rw, rh)
            ctx.strokeStyle = fillStyle
            var corner = bmargin/4

            if(margin == 0)
            // draw corners around the emphasize rectangle
            // to better see it inside low contrast clut areas
            {
                ctx.beginPath()
                ctx.moveTo(rx, ry + corner)
                ctx.lineTo(rx, ry)
                ctx.stroke()
                ctx.beginPath()
                ctx.moveTo(rx+rw, ry+rh - corner)
                ctx.lineTo(rx+rw, ry+rh)
                ctx.stroke()
                ctx.beginPath()
                ctx.lineTo(rx+rw, ry)
                ctx.lineTo(rx+rw, ry+corner)
                ctx.stroke()
                ctx.beginPath()
                ctx.moveTo(rx, ry+rh-corner)
                ctx.lineTo(rx, ry+rh)
                ctx.stroke()

                ctx.strokeStyle = strokeStyle
                ctx.beginPath()
                ctx.lineTo(rx, ry)
                ctx.lineTo(rx + corner, ry)
                ctx.stroke()
                ctx.beginPath()
                ctx.lineTo(rx+rw, ry+rh)
                ctx.lineTo(rx+rw - corner, ry+rh)
                ctx.stroke()
                ctx.beginPath()
                ctx.moveTo(rx+rw-corner, ry)
                ctx.lineTo(rx+rw, ry)
                ctx.stroke()
                ctx.beginPath()
                ctx.lineTo(rx, ry+rh)
                ctx.lineTo(rx+corner, ry+rh)
                ctx.stroke()
            }

            if(vctx.clut.value_in === 3)
            // draw curves orthogonal to actual focus
            {
                ctx.lineWidth = lineWidth/2.0
                var rsize = tm/7
                for(i = 0; i < vctx.clut.value_out; ++i)
                {
                    x = rdl
                    xy_pos = 0*vctx.grid[1]*vctx.grid[0]*vctx.clut.value_out + vctx.coord[1]*vctx.grid[0]*vctx.clut.value_out + vctx.coord[0]*vctx.clut.value_out
                    cname = vctx.clut.values_channels_names_out[i]
                    colors = colorFromChannelName(cname, i)
                    ctx.strokeStyle = colors[2]
                    ctx.beginPath()
                    ctx.moveTo(x, d-d*vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]])
                    var j
                    for(j = 1; j < vctx.grid[2]; ++j)
                    {
                        xy_pos = j*vctx.grid[1]*vctx.grid[0]*vctx.clut.value_out + vctx.coord[1]*vctx.grid[0]*vctx.clut.value_out + vctx.coord[0]*vctx.clut.value_out
                        x = rdl + j/vctx.grid[2]*rdw
                        ctx.lineTo(x, d-d*vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]])
                    }
                    ctx.stroke()

                    j = vctx.coord[2]
                    x = rdl + j/vctx.grid[2]*rdw - rsize/2
                    xy_pos = j*vctx.grid[1]*vctx.grid[0]*vctx.clut.value_out + vctx.coord[1]*vctx.grid[0]*vctx.clut.value_out + vctx.coord[0]*vctx.clut.value_out
                    ctx.fillRect(x, d-d*vctx.clut.data.value_array[xy_pos+vctx.channels_out[i]]-rsize/2, rsize, rsize)
                }
            }
        } else
        {
            chanButtonsRow.visible = false
            coordLabel.visible = false
        }

        textMetrics.font = ctx.font
    }

    function drawDecoration( ctx, projection_xyz )
    {
        //ctx.rect(-lm, -head, d, d)
        //ctx.fill()

        //! decoration begin
        if(projection_xyz) {
            ctx.beginPath()
            ctx.moveTo(-sm/4, 0)
            ctx.lineTo(0,0)
            ctx.lineTo(0,d)
            ctx.lineTo(d,d)
            ctx.lineTo(    d,d+sm/4)
            ctx.stroke()
        } else {
            ctx.beginPath()
            ctx.moveTo(0,d/2)
            ctx.lineTo(d,d/2)
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(d/2,0)
            ctx.lineTo(d/2,d)
            ctx.stroke()
        }

        var lineWidth = ctx.lineWidth;
        ctx.lineWidth = lineWidth/8.0
        for(var i = 0.0; i <= 10.0; ++i)
        {
            ctx.beginPath()
            ctx.moveTo(0, d-d/10*i)
            ctx.lineTo(d, d-d/10*i)
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(d/10*i,0)
            ctx.lineTo(d/10*i,d)
            ctx.stroke()
        }
        ctx.lineWidth = lineWidth
    }


    /* spectal variables */
    property int nano_min: 63  /* 420 nm */
    property int nano_max: 340 /* 699 nm */
    function xToImage(val) { return val * d }
    function yToImage(val) { return val * d * -1.0 + d }
    function xFromXYZ(XYZ, projection_xyz) {
        if(projection_xyz) {
            return XYZ[0] / (XYZ[0] + XYZ[1] + XYZ[2])
        } else {
            var lab = Color.oyXYZ2Lab(XYZ)
            return lab[1]/256.0 + 0.5
        }
    }
    function yFromXYZ(XYZ, projection_xyz) {
        if(projection_xyz) {
            return XYZ[1] / (XYZ[0] + XYZ[1] + XYZ[2])
        } else {
            var lab = Color.oyXYZ2Lab(XYZ)
            return lab[2]/256.0 + 0.5
        }
    }

    function drawSpectra( ctx, projection )
    {
      var i = nano_min
      ctx.beginPath()
      ctx.moveTo( xToImage( xFromXYZ(Color.cieXYZ_31_2[i], projection)),
                  yToImage( yFromXYZ(Color.cieXYZ_31_2[i], projection)))
      for(i = nano_min+1; i < nano_max; ++i)
        ctx.lineTo( xToImage( xFromXYZ(Color.cieXYZ_31_2[i], projection)),
                    yToImage( yFromXYZ(Color.cieXYZ_31_2[i], projection)))
      i = nano_min
      ctx.lineTo( xToImage( xFromXYZ(Color.cieXYZ_31_2[i], projection)),
                  yToImage( yFromXYZ(Color.cieXYZ_31_2[i], projection)))
      ctx.stroke()
    }

    function drawSaturation( ctx, projection )
    {
      var i = 0
      // is already white point scaled by the absolute rendering intent
      var s = JSON.parse( saturationLine )

      if( typeof s.data === "undefined" ||
          typeof s.data.value_array === "undefined" )
          return 1

      ctx.beginPath()
      ctx.moveTo( xToImage( xFromXYZ(s.data.value_array[i], projection)),
                  yToImage( yFromXYZ(s.data.value_array[i], projection)))
      for(i = 1; i < s.data.count; ++i)
        ctx.lineTo( xToImage( xFromXYZ(s.data.value_array[i], projection)),
                    yToImage( yFromXYZ(s.data.value_array[i], projection)))
      i = 0
      ctx.lineTo( xToImage( xFromXYZ(s.data.value_array[i], projection)),
                  yToImage( yFromXYZ(s.data.value_array[i], projection)))
      return 0
    }

    property real tm: textArea.font.pixelSize * 2 // top margin
    property real head: Math.max( tagView.height + tm, tm*4 > canvas.height ? tm : 2*tm)
    property real d: Math.min( canvas.width - 2*tm, canvas.height - head-1.5*tm ) // extent
    property real sm: textArea.font.pixelSize // side margin
    property real lm: textArea.font.pixelSize // left margin
    property real rdl: d + tm/8 // right draw area left start
    property real rdw: canvas.width - rdl - tm // right draw area with
    property int doDrawCurve: 0
    property int skipHeadline: 0
    property bool projection_xyz: false
    Canvas {
        id: canvas
        width: parent.width
        height: parent.height - tm
        property color strokeStyle:  Qt.darker(fillStyle, 1.4)
        property color fillStyle: "#b4b4b4" // gray

        property real lineWidth: textArea.font.pixelSize / 5.0
        antialiasing: true

        onPaint: {
            doDrawCurve = 0
            skipHeadline = 0

            if(typeof viewJson === "undefined")
                return

            var ctx = canvas.getContext('2d')
            ctx.clearRect(0, 0, canvas.width, canvas.height)

            if(showJson === false)
                ctx.globalAlpha = 1
            else
                ctx.globalAlpha = 0.3

            var json = viewJson
            if(viewJson.type === "XYZArray" ||
               viewJson.type === "XYZ " ||
               viewJson.type === "chromaticity")
                drawXYZ(ctx, canvas.strokeStyle, canvas.fillStyle, lineWidth)

            if(viewJson.type === "curve" ||
               viewJson.type === "pcurve")
                doDrawCurve = 1
            else
                doDrawCurve = 0
            if(doDrawCurve)
            {
                ctx.save()

                ctx.strokeStyle = canvas.strokeStyle
                ctx.fillStyle = canvas.fillStyle
                ctx.lineWidth = lineWidth/2.0
                ctx.translate( lm, height - d - 2.5*sm )

                drawDecoration(ctx, true)

                //! all curves
                drawCurve(ctx, curve_red, "", -1, textColor)
                drawCurve(ctx, curve_green, "", -1, textColor)
                drawCurve(ctx, curve_blue, "", -1, textColor)
                drawCurve(ctx, curve_gray, "", -1, textColor)
                //! end all curves

                var textColor = canvas.fillStyle
                if(viewJson.key === "redTRC")
                    textColor = ctx.strokeStyle = "red"
                else if(viewJson.key === "greenTRC")
                    textColor = ctx.strokeStyle = "green"
                else if(viewJson.key === "blueTRC")
                {
                    ctx.strokeStyle = "blue"
                    textColor = Qt.lighter("blue", 1.3)
                }
                else if(viewJson.key === "grayTRC")
                    textColor = ctx.strokeStyle = "black"

                drawCurve(ctx, viewJson, viewJson.key_i18n, 0, textColor )

                ctx.restore()
            }

            if(viewJson.type === "curves")
            {
                if(typeof viewJson.subelements_compound === "undefined")
                    skipHeadline = 1
                doDrawCurve = 1

                ctx.save()

                ctx.strokeStyle = canvas.strokeStyle
                ctx.fillStyle = canvas.fillStyle
                ctx.lineWidth = lineWidth/2.0
                ctx.translate( lm, height - d - 2.5*sm )

                drawDecoration(ctx, true)

                drawCurves(ctx, viewJson, 1)

                ctx.restore()
            }

            if(viewJson.key === "clut")
            {
                skipHeadline = 1
                doDrawCurve = 1
                ctx.save()
                ctx.translate( lm, height - d - 2.5*sm )
                drawDecoration(ctx, true)

                drawClut(ctx, lineWidth, fillStyle, strokeStyle)

                ctx.restore()
            } else {
                slider.visible = false
                channelBox.visible = false
                chanButtonsRow.visible = false
                coordLabel.visible = false
            }

            parseMyJson( viewJson )
        }
    }
    property real pos_x
    property real pos_y
    property bool inside // inside drawCurveMouseArea
    property bool drawCurveMouseAreaClicked: false
    MouseArea {
        id: drawCurveMouseArea
        x: lm
        y: parent.height - d - 2.5*sm - tm
        width: d
        height: doDrawCurve * d
        hoverEnabled: true
        onPositionChanged: {
            if(!drawCurveMouseAreaClicked)
            {
                pos_x = mouse.x
                pos_y = mouse.y
            }
            inside = true
            if(doDrawCurve)
                canvas.requestPaint()
        }
        /* disabled because of side effect of missing swipe gesture  onExited: { // reduce non needed redraws
            inside = drawCurveMouseAreaClicked
            repaint = !repaint
        }*/
        onClicked: drawCurveMouseAreaClicked = (!fullscreen) && !drawCurveMouseAreaClicked
        preventStealing: true // do not forward events to other layers, supress unwanted swipe on Android
    }
}

