/** @file icc_profile.h
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2014/12/08
 *
 *  profile load and C++ <-> QML wrappers
 */

#ifndef ICC_PROFILE_H
#define ICC_PROFILE_H

#include <QObject>
#include <QByteArray>
#include <QJsonObject>

#include "utils.h"
#include "icc_download.h"

typedef struct oyProfile_s oyProfile_s;

class IccProfile : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString fileName
               READ fileName
               WRITE setFileName
               NOTIFY fileNameChanged
               REVISION 1)
    Q_PROPERTY(QString quitMsg
               READ quitMsg
               WRITE setQuitMsg
               NOTIFY quitMessage
               REVISION 1)
    Q_PROPERTY(QString log
               READ log
               WRITE setLog
               REVISION 1)
    IccDownload iccDownload;
public:
    IccProfile(QObject * parent = 0) : QObject(parent) {
        m_p = NULL;
        QObject::connect( &iccDownload, SIGNAL( downloadFinished( QByteArray, QString ) ),
                          this, SLOT( setBytes( QByteArray, QString ) ) );
    }

    QString fileName() {
        return m_file_name;
    }
    void setFileName( QString fn ) {
        m_file_name = fn;
        m_bytes.clear();
        clearProfile();

        // test for http Uri
        QUrl url;
        url.setUrl( m_file_name );
        if(url.scheme() == "http")
        {
            LOG( QString("call to iccDownload.url(") + url.toString() + ")" );
            iccDownload.url(url);
            LOG( QString("call to iccDownload.url(url) done") );

            // the iccDownload will cal into this object later and emit a signal
            return;
        }

        LOG( fn );
        emit fileNameChanged();
        LOG( QString("emit fileNameChanged() signal emitted") );
    }
    QString quitMsg() { return m_msg; }
    void setQuitMsg( QString m ) {
        m_msg = m;
        LOG( m_msg );
        emit quitMessage();
        LOG( QString("quitMessage() signal emitted") );
    }

    QString log() { return m_log_msg; }
    void setLog( QString m ) {
        m_log_msg = m;
        LOG( m_log_msg );
    }

    QByteArray bytes() {
        return m_bytes;
    }
    Q_INVOKABLE QString getDescription();
    Q_INVOKABLE QString getJSON();
    Q_INVOKABLE QString getSaturationLine();
    Q_INVOKABLE QString getLibDescription(int);
    Q_INVOKABLE QVariantList getAbsoluteXYZ(QVariantList rgb);
    Q_INVOKABLE QVariantList getColor(QVariantList lab, QString profile_name);
    Q_INVOKABLE QString getProfileList();
public slots:
    void setBytes( QByteArray b, QString name ) {
        m_bytes = b;
        m_file_name = name;
        LOG( QString::number(m_bytes.size()) + " " + name );
        emit fileNameChanged();
        LOG( QString("fileNameChanged() signal emitted") );
    }

signals:
    void fileNameChanged();
    void quitMessage();
private:
    QString m_file_name;
    QByteArray m_bytes;
    QString m_msg;
    QString m_log_msg;
    oyProfile_s * m_p;
    void load_();
    void clearProfile();
};

#endif // ICC_PROFILE_H
