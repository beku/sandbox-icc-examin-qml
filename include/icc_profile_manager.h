/** @file icc_profile_manager.h
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2015/01/06
 *
 *  independent file load instance
 */

#ifndef ICC_PROFILE_MANAGER_H
#define ICC_PROFILE_MANAGER_H
#include "icc_profile.h"
#include <QUrl>

class IccProfileManager : public QObject
{
    Q_OBJECT
    IccProfile * profile_;
    QString uri_;
    QByteArray mem_;

public:
    IccProfileManager(QObject * parent = 0) : QObject(parent) { profile_ = 0; }
    ~IccProfileManager() { profile_ = 0; }
    // store the profile pointer and pass in a possibly available uri from before main()
    void setProfile(IccProfile * icc)
    {
        profile_ = icc;
        if( profile_ )
        {
            if( uri_.size() )
                profile_->setFileName( uri_ );
            if(mem_.size())
                profile_->setBytes( mem_, "----" );
        }
    }

    // keep uri around (before main() call) or send uri to QML profile
    void setUri( QString uri )
    {
        LOG( uri + ((profile_!=0)?" profile_ ready":" profile_ not yet created") );

        if(profile_)
            profile_->setFileName(uri);
        else
            uri_ = uri;
    }
    IccProfile * getProfile() { return profile_; }

public slots:
    void setMem( QByteArray data, QUrl url )
    {
        LOG( QString::number(data.size()) );

        if(profile_)
            profile_->setBytes( data, url.toString() );
        else
            mem_ = data;
        uri_.clear();
    }
};

#endif // ICC_PROFILE_MANAGER_H

