/** @file utils.h
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2014/12/13
 *
 *  helpers
 */

#ifndef UTILS_H
#define UTILS_H

#include <QString>

void icc_examin_log( QString s );
void icc_examin_log( QString file, QString func, QString log_text );

#define LOG( text ) icc_examin_log( QString(__FILE__) + ":" + QString::number(__LINE__), QString(__func__) + "()", text )

int iccExaminIsBigEndian ();

#endif // UTILS_H
