# ICC Examin
[![Pipeline](https://gitlab.com/oyranos/icc-examin-qml/badges/master/pipeline.svg)](https://gitlab.com/oyranos/icc-examin-qml/-/pipelines)
[![Documentation](images/tool-documented.svg)](docs/md/iccExaminQml.md)
[![License: AGPL v3](images/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

![Logo](images/logo-color.svg)

A ICC v2 and v4 color profile viewer. The ICC Examin color examination tool accepts as input ICC profiles, CGATS color measurement files and named color files.

### Links
* [Copyright](docs/COPYING) - AGPL-3.0 (Affero GPL)
* [Author](https://gitlab.com/beku)

### Dependencies
* [RefIccMAX](http://github.com/InternationalColorConsortium/RefIccMAX) - a ICC parser library
* [Oyranos](https://gitlab.com/oyranos/oyranos) - CMS

### Building
Supported are Qt Designer and qmake style builds.

Open the pro file in Qt Designer, configure and add the desired kits.

Point qmake in Qt Creator's project settings to the desired libIccProfLib.a :

* qmake build step additional args: LIBS+=-lIccProfLib

Add two extra build step to generate the translation qm files:

* command: lupdate
  * args: -pro ICCExamin.pro -ts translations/iccExamin\_de.ts
  * working in: %{sourceDir}
* command: lrelease
  * args: ICCExamin.pro
  * working in: %{sourceDir}

#### For Android install ant, otherwise use gradle.
* Cross Compile for Android
  * set CMAKE\_SYSROOT /path\_to/android-ndk-r11c/platforms/android-24/arch-arm
  * set CMAKE\_SYSTEM\_NAME : Android - compile target
  * set CMAKE\_FIND\_ROOT\_PATH : /opt/arm-linux-androideabi-4.9 - path to other toolchain libs

#### Static linking with qmake, e.g. Oyranos on Linux
* get the libraries from the cmake project
* convert to qmake LIBS+= entries

##### Example linux bash
      LIB_PATH=/home/kuwe/.local/lib64; LIB_PATH2=/usr/lib64; echo 'm;c;yajl;/usr/lib64/libxml2.so;lcms2;raw;stdc++;lcms2;png16;/usr/lib64/libz.so;/usr/lib64/libjpeg.so;xcm-static;Xfixes;xcm-static;Xfixes;/home/kuwe/.local/lib64/libOpenICC.so;-lelektra-core;-lelektra-kdb;Xinerama;Xrandr;xcm-static;Xfixes;Xxf86vm;/usr/lib64/libSM.so;/usr/lib64/libICE.so;X11;/usr/lib64/libX11.so;/usr/lib64/libXext.so;raw;stdc++;lcms2;/usr/lib64/libexiv2.so;/usr/lib64/libcups.so;/usr/lib64/libsane.so' | sed "s%$LIB_PATH/lib%%g" | sed "s%$LIB_PATH2/lib%%g" | sed 's/[.]so//g; s/;-l/ LIBS+=-l/g; s/;\//\ \//g; s/;/\ LIBS+=-l/g; s%\ %\n%g; s/^/LIBS+=-l/; s/ //g' | sort -u | xargs
